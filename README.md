# Biassess Challenge

The project is pretty simple, it would be just a single page JavaScript app that handles authentication via Facebook OAuth. You can complete it any way you'd like, as long as the app functions to the basic req's. What you want to build is entirely up to you.

### Requirements
1. Use ReactJS and Redux
2. User should successfully log in and persist authentication with OAuth
3. User should be able to successfully log out
4. If logged in, going to root should see a user’s profile information
5. If not logged in, going to root should see the log in page
6. Do not use any 3rd party libraries (passport, etc)

### Notes
* Minimal styling is fine, be as creative as you’d like
* Bonus points if it looks good and works well across different screen sizes
* Solution does not need to support older browsers
