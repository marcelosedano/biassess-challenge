import { css, keyframes } from 'styled-components';

const fadeIn = keyframes`
  from { opacity: 0; }
  to   { opacity: 1; }
`;

export default duration => css`
  animation: ${fadeIn} ${duration};
`;
