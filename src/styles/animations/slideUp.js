import { css, keyframes } from 'styled-components';

const slideUp = keyframes`
  0% {
    transform: translateY(100%);
  }
  50%{
    transform: translateY(-4%);
  }
  80%{
    transform: translateY(1%);
  }
  100% {
    transform: translateY(0%);
  }
`;

export default css`
  animation: ${slideUp} 1s ease;
`;
