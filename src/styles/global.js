import { injectGlobal } from 'styled-components';

// Set box-sizing globally to handle padding and border widths
const all = `
  *,
  *:after,
  *:before {
    box-sizing: border-box;
  }

  * {
    background-repeat: no-repeat;
  }
`;

// The base font-size is set at 62.5% for having the convenience
// of sizing rems in a way that is similar to using px: 1.6rem = 16px
const html = `
  html {
    box-sizing: border-box;
    background: linear-gradient(to right, #6cadff 0%, #3876ff 75%);
    font-size: 62.5%;
    cursor: default;
    overflow-x: hidden;
  }
`;

const font = `
  @font-face {
    font-family: 'Montserrat';
    src: url('static/fonts/montserrat-v12-latin-regular.woff2') format('woff2'),
         url('static/fonts/montserrat-v12-latin-regular.woff') format('woff'),
         url('static/fonts/montserrat-v12-latin-regular.ttf') format('truetype');
    font-weight: 400;
  }

  @font-face {
    font-family: 'Montserrat';
    src: url('static/fonts/montserrat-v12-latin-600.woff2') format('woff2'),
         url('static/fonts/montserrat-v12-latin-600.woff') format('woff'),
         url('static/fonts/montserrat-v12-latin-600.ttf') format('truetype');
    font-weight: 600;
  }

  @font-face {
    font-family: 'Montserrat';
    src: url('static/fonts/montserrat-v12-latin-800.woff2') format('woff2'),
         url('static/fonts/montserrat-v12-latin-800.woff') format('woff'),
         url('static/fonts/montserrat-v12-latin-800.ttf') format('truetype');
    font-weight: 800;
  }

  body,
  input,
  textarea,
  select,
  button {
    font-family: Montserrat, Helvetica Neue, Helvetica, Arial, san-serif;
    font-size: 1.6rem;
    line-height: 1.2;
    letter-spacing: .01em;
    color: white;
    -webkit-font-smoothing: antialiased;
  }
`;

const body = `
  body {
    margin: 0;
  }
`;

export const injectGlobalStyles = () => injectGlobal`
  ${all};
  ${html};
  ${font};
  ${body};
`;
