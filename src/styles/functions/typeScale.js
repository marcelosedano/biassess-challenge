import { css } from 'styled-components';
import type from '../constants/type';

export default typeName => css`
  font-size: ${type.scale[typeName].size};
  line-height: ${type.scale[typeName].lineHeight};
`;
