import styled from 'styled-components';
import type from 'styles/constants/type';
import pulse from 'styles/animations/pulse';
import typeScale from 'styles/functions/typeScale';

export default styled.button`
  ${typeScale('milli')}
  font-weight: ${type.weight.semiBold};
  letter-spacing: .1rem;
  background: linear-gradient(to right, #6cadff 0%, #3876ff 75%);
  padding: 2rem 3.2rem;
  color: white;
  border-radius: 4.5rem;
  border: 0;
  text-transform: uppercase;
  cursor: pointer;
  outline: 0;

  &:hover {
    ${pulse}
  }
`;
