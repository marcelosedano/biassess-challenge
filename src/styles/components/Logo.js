import styled from 'styled-components';

export default styled.div`
  font-size: 8.2rem;
  line-height: 5.8rem;
  font-weight: 600;
  letter-spacing: .15em;
`;
