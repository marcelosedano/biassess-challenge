import styled from 'styled-components';

export default styled.a`
  color: #3876ff;
  text-decoration: none;

  &:hover {
    color: #2c5ecc;
  }
`;
