import styled from 'styled-components';
import type from 'styles/constants/type';
import typeScale from 'styles/functions/typeScale';

export default styled.button`
  ${typeScale('milli')}
  font-weight: ${type.weight.semiBold};
  letter-spacing: .1rem;
  background: white;
  padding: 2rem 3.2rem;
  color: #4584ff;
  border-radius: 4.5rem;
  border: .2rem solid white;
  text-transform: uppercase;
  cursor: pointer;
  outline: 0;

  &:hover {
    background: rgba(255, 255, 255, 0.2);
    border: .2rem solid white;
    color: white;
  }
`;
