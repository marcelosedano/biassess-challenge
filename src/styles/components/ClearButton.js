import styled from 'styled-components';

/**
 * This button component can be used to wrap a non-button element to support accessibility
 */
export default styled.button`
  background-color: transparent;
  border: 0;
  color: inherit;
  cursor: pointer;
  font: inherit;
  text-transform: inherit;
  padding: 0;
`;
