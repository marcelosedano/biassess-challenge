import { css } from 'styled-components';

const sizes = {
  max: 1440,
  desktop: 1020,
  tablet: 735,
  mobileLarge: 375,
  mobile: 320,
  baseline: 240,
};

const media = {
  max: {
    baseline: (...args) => css`
      @media (max-width: ${(sizes.mobile - 1) / 16}em) {
        ${css(...args)};
      }
    `,
    desktop: (...args) => css`
      @media (max-width: ${(sizes.max - 1) / 16}em) {
        ${css(...args)};
      }
    `,
    mobile: (...args) => css`
      @media (max-width: ${(sizes.mobileLarge - 1) / 16}em) {
        ${css(...args)};
      }
    `,
    mobileLarge: (...args) => css`
      @media (max-width: ${(sizes.tablet - 1) / 16}em) {
        ${css(...args)};
      }
    `,
    tablet: (...args) => css`
      @media (max-width: ${(sizes.desktop - 1) / 16}em) {
        ${css(...args)};
      }
    `,
  },
  min: {
    baseline: (...args) => css`
      @media (min-width: ${sizes.min / 16}em) {
        ${css(...args)};
      }
    `,
    desktop: (...args) => css`
      @media (min-width: ${sizes.desktop / 16}em) {
        ${css(...args)};
      }
    `,
    max: (...args) => css`
      @media (min-width: ${sizes.max / 16}em) {
        ${css(...args)};
      }
    `,
    mobile: (...args) => css`
      @media (min-width: ${sizes.mobile / 16}em) {
        ${css(...args)};
      }
    `,
    mobileLarge: (...args) => css`
      @media (min-width: ${sizes.mobileLarge / 16}em) {
        ${css(...args)};
      }
    `,
    tablet: (...args) => css`
      @media (min-width: ${sizes.tablet / 16}em) {
        ${css(...args)};
      }
    `,
  },
};

export default media;
