export default {
  weight: {
    regular: 400,
    semiBold: 600,
    bold: 800,
  },
  scale: {
    milli: {
      size: '1.2rem',
      lineHeight: `${16 / 12}rem`,
    },
    centi: {
      size: '1.4rem',
      lineHeight: `${20 / 14}rem`,
    },
    uno: {
      size: '1.6rem',
      lineHeight: `${24 / 16}rem`,
    },
    hecto: {
      size: '1.8rem',
      lineHeight: `${26 / 18}rem`,
    },
    kilo: {
      size: '2.1rem',
      lineHeight: `${28 / 21}rem`,
    },
    giga: {
      size: '2.4rem',
      lineHeight: `${32 / 24}rem`,
    },
    peta: {
      size: '3.2rem',
      lineHeight: `${44 / 32}rem`,
    },
    yotta: {
      size: '4.7rem',
      lineHeight: `${60 / 47}rem`,
    },
    bronto: {
      size: '5.4rem',
      lineHeight: `${72 / 54}rem`,
    },
  },
};
