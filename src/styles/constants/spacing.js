export default {
  slim: '.4rem',
  cozy: '.8rem',
  compact: '1.2rem',
  moderate: '1.6rem',
  normal: '2.4rem',
  expanded: '3.2rem',
  spacious: '4.4rem',
  giant: '6.4rem',
  colossal: '8.8rem',
};
