import React from 'react';
import PropTypes from 'prop-types';
import HomeView from '../HomeView';
import LoadingView from '../LoadingView';
import LoginView from '../LoginView';
import NavigationMenu from '../NavigationMenu';
import { Wrapper } from './style';

const App = ({ isLoading, isLoggedIn }) => {
  let content = <LoadingView />;
  if (!isLoading) {
    content = isLoggedIn ? <HomeView /> : <LoginView />;
  }

  return (
    <Wrapper>
      <NavigationMenu />
      {content}
    </Wrapper>
  );
};

App.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
};

export default App;
