import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getIsLoggedIn, getIsLoading } from 'store/selectors';
import { restoreSession } from 'store/actions';
import App from './App';

const mapStateToProps = state => ({
  isLoading: getIsLoading(state),
  isLoggedIn: getIsLoggedIn(state),
});

const mapDispatchToProps = dispatch => ({
  restoreSession: () => dispatch(restoreSession()),
});

class AppContainer extends Component {
  componentDidMount() {
    window.fbLoaded.promise.then(() => {
      this.props.restoreSession();
    });
  }

  render() {
    return <App {...this.props} />;
  }
}

AppContainer.propTypes = {
  restoreSession: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
