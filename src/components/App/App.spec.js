/* eslint-env jest */

import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import App from './App';

describe('App', () => {
  const renderer = new ShallowRenderer();

  it('renders correctly when the app is loading', () => {
    renderer.render(<App isLoading isLoggedIn />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();

    renderer.render(<App isLoading isLoggedIn={false} />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();
  });

  it('renders correctly when the app is not loading and the user is not logged in', () => {
    renderer.render(<App isLoading={false} isLoggedIn={false} />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();
  });

  it('renders correctly when the app is not loading and the user is logged in', () => {
    renderer.render(<App isLoading={false} isLoggedIn />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();
  });
});
