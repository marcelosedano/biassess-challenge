import styled from 'styled-components';
import media from 'styles/constants/media';
import typeScale from 'styles/functions/typeScale';
import fadeIn from 'styles/animations/fadeIn';

export const Wrapper = styled.div`
  height: 35vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  ${fadeIn('1s')}
`;

export const SecondaryText = styled.div`
  ${typeScale('hecto')}
  text-align: center;

  ${media.max.mobileLarge`
    ${typeScale('centi')}
  `};

  ${media.max.mobile`
    ${typeScale('milli')}
  `};
`;

export const MainTextWrapper = styled.div`
  text-align: center;
`;

export const MainText = styled.div`
  ${typeScale('yotta')}
  font-weight: 600;

  ${media.min.tablet`
    font-size: 8.2rem;
    line-height: 5.8rem;
  `};
`;

export const ButtonWrapper = styled.div`
  text-align: center;
`;
