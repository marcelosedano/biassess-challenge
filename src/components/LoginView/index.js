import { connect } from 'react-redux';
import { login } from 'store/actions';
import LoginView from './LoginView';

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
  onLogin: () => dispatch(login()),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);
