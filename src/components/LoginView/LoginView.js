import React from 'react';
import PropTypes from 'prop-types';
import Button from 'styles/components/Button';
import {
  ButtonWrapper,
  MainText,
  MainTextWrapper,
  SecondaryText,
  Wrapper,
} from './style';

const LoginView = ({ onLogin }) => (
  <Wrapper>
    <MainTextWrapper>
      <MainText>Welcome</MainText>
    </MainTextWrapper>
    <SecondaryText>This is the take-home challenge for Biassess</SecondaryText>
    <ButtonWrapper>
      <Button onClick={onLogin}>Login with Facebook</Button>
    </ButtonWrapper>
  </Wrapper>
);

LoginView.propTypes = {
  onLogin: PropTypes.func.isRequired,
};

export default LoginView;
