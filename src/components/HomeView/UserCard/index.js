import { connect } from 'react-redux';
import { getIsViewingReport, getUser } from 'store/selectors';
import { toggleViewingReport } from 'store/actions';
import UserCard from './UserCard';

const mapStateToProps = state => ({
  isViewingReport: getIsViewingReport(state),
  user: getUser(state),
});
const mapDispatchToProps = dispatch => ({
  onViewReportClick: () => dispatch(toggleViewingReport()),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserCard);
