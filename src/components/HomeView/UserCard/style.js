import styled from 'styled-components';
import media from 'styles/constants/media';
import slideUp from 'styles/animations/slideUp';
import fadeIn from 'styles/animations/fadeIn';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 90%;
  ${slideUp}

  ${media.min.tablet`
    ${fadeIn('0.5s')}
  `};

  ${media.min.desktop`
    width: 900px;
  `};
`;
