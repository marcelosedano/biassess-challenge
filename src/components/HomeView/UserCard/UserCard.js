import React from 'react';
import PropTypes from 'prop-types';
import Picture from './Picture';
import Card from './Card';
import { Wrapper } from './style';

const UserCard = ({
  isViewingReport,
  onViewReportClick,
  user: { picture, ...userInfo },
}) => (
  <Wrapper>
    <Picture picture={picture.data} />
    <Card
      isViewingReport={isViewingReport}
      onViewReportClick={onViewReportClick}
      userInfo={userInfo}
    />
  </Wrapper>
);

UserCard.propTypes = {
  isViewingReport: PropTypes.bool.isRequired,
  onViewReportClick: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default UserCard;
