import React from 'react';
import PropTypes from 'prop-types';
import { Wrapper, Image } from './style';

const Picture = ({ picture }) => (
  <Wrapper>
    <Image
      src={picture.url}
      width={picture.width}
      height={picture.height}
      alt="user"
    />
  </Wrapper>
);

Picture.propTypes = {
  picture: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default Picture;
