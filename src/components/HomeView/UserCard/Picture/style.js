import styled from 'styled-components';
import media from 'styles/constants/media';

export const Wrapper = styled.div`
  height: 50px;
  z-index: 1;
  display: flex;
  justify-content: center;
  width: 100%;

  ${media.min.tablet`
    padding-left: 32px;
    justify-content: flex-start;
  `};
`;

export const Image = styled.img`
  border-radius: 6px;
`;
