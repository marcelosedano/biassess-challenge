/* eslint-env jest */

import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import Card from './index';

describe('Card', () => {
  const renderer = new ShallowRenderer();
  const requiredProps = {
    onViewReportClick: () => {},
    userInfo: {},
  };

  it('renders correctly when the user is not viewing the report', () => {
    renderer.render(<Card isViewingReport={false} {...requiredProps} />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();
  });

  it('renders correctly when the user is viewing the report', () => {
    renderer.render(<Card isViewingReport {...requiredProps} />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();
  });
});
