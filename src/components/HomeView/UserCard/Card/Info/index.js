import React from 'react';
import PropTypes from 'prop-types';
import Link from 'styles/components/Link';
import {
  BioText,
  EmailText,
  InfoItem,
  NameText,
  Text,
  Wrapper,
} from './style';

const Info = ({ userInfo }) => (
  <Wrapper>
    <InfoItem>
      <NameText>{userInfo.name}</NameText>
    </InfoItem>
    <InfoItem>
      <Text>Entrepreneur at Biassess</Text>
    </InfoItem>
    <InfoItem>
      <BioText><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></BioText>
    </InfoItem>
    <InfoItem>
      <EmailText><Link href={`mailto:${userInfo.email}`}>{userInfo.email}</Link></EmailText>
    </InfoItem>
  </Wrapper>
);

Info.propTypes = {
  userInfo: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default Info;
