import styled from 'styled-components';
import { media, type } from 'styles/constants';
import fadeIn from 'styles/animations/fadeIn';
import typeScale from 'styles/functions/typeScale';

export const Wrapper = styled.div`
  padding-top: 12px;
  display: flex;
  flex-direction: column;
  align-items: center;
  ${fadeIn('0.5s')}

  ${media.min.tablet`
    padding-top: 105px;
    align-items: flex-start;
  `};
`;

export const InfoItem = styled.div`
  padding: 8px 0;
`;

export const Text = styled.span`
  ${media.max.mobile`
    ${typeScale('centi')}
  `};
`;

export const BioText = styled(Text)`
  color: #787c80;
`;

export const EmailText = styled(Text)`
  font-weight: ${type.weight.semiBold};
`;

export const NameText = styled.span`
${typeScale('peta')}
  font-weight: ${type.weight.semiBold};

  ${media.max.mobileLarge`
    ${typeScale('kilo')}
  `};

  ${media.max.mobile`
    ${typeScale('hecto')}
  `};
`;
