import styled from 'styled-components';
import media from 'styles/constants/media';

export const Wrapper = styled.div`
  background-color: white;
  border-radius: 6px;
  padding: 32px;
  filter: drop-shadow(6px 6px 4px rgba(0, 0, 0, 0.2));
  color: #32373b;
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;

  ${media.max.mobileLarge`
    justify-content: center;
    padding-top: 140px;
  `};
`;

export const CTAButtonWrapper = styled.div`
  float: right;
`;
