import React from 'react';
import PropTypes from 'prop-types';
import CTAButton from 'styles/components/CTAButton';
import Info from './Info';
import Report from './Report';
import {
  ButtonContainer,
  CTAButtonWrapper,
  Wrapper,
} from './style';

const Card = ({
  isViewingReport,
  onViewReportClick,
  userInfo,
}) => {
  const buttonText = isViewingReport ? 'Back to Profile' : 'View Report';
  return (
    <Wrapper>
      <ButtonContainer>
        <CTAButtonWrapper>
          <CTAButton onClick={onViewReportClick}>{buttonText}</CTAButton>
        </CTAButtonWrapper>
      </ButtonContainer>
      {isViewingReport
        ? <Report />
        : <Info userInfo={userInfo} />
      }
    </Wrapper>
  );
};

Card.propTypes = {
  isViewingReport: PropTypes.bool.isRequired,
  onViewReportClick: PropTypes.func.isRequired,
  userInfo: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default Card;
