import styled from 'styled-components';
import fadeIn from 'styles/animations/fadeIn';

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 500px;
  ${fadeIn('0.5s')}
`;
