import React from 'react';
import UserCard from './UserCard';
import { Wrapper } from './style';

const HomeView = () => (
  <Wrapper>
    <UserCard />
  </Wrapper>
);

export default HomeView;
