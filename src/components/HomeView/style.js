import styled from 'styled-components';

export const Wrapper = styled.div`
  padding-top: 100px;
  padding-bottom: 20px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;
