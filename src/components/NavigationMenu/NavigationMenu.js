import React from 'react';
import PropTypes from 'prop-types';
import ClearButton from 'styles/components/ClearButton';
import {
  Item,
  NavLink,
  NavList,
  Wrapper,
} from './style';

const NavigationMenu = ({ isLoggedIn, onLogout }) => (
  isLoggedIn ? (
    <Wrapper>
      <NavList>
        <Item>
          <NavLink href="https://gitlab.com/marcelosedano/biassess-challenge" target="_blank">
            Gitlab
          </NavLink>
        </Item>
        <Item>
          <ClearButton onClick={onLogout}>Logout</ClearButton>
        </Item>
      </NavList>
    </Wrapper>
  ) : null
);

NavigationMenu.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  onLogout: PropTypes.func.isRequired,
};

export default NavigationMenu;
