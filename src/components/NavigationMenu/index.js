import { connect } from 'react-redux';
import { logout } from 'store/actions';
import { getIsLoggedIn } from 'store/selectors';
import NavigationMenu from './NavigationMenu';

const mapStateToProps = state => ({
  isLoggedIn: getIsLoggedIn(state),
});
const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NavigationMenu);
