import styled from 'styled-components';
import type from 'styles/constants/type';
import pulse from 'styles/animations/pulse';
import typeScale from 'styles/functions/typeScale';
import Link from 'styles/components/Link';
import List from 'styles/components/List';

export const Wrapper = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  padding: 4rem 2rem 0 0;
  font-weight: ${type.weight.semiBold};
  letter-spacing: .15rem;
  ${typeScale('centi')}
  z-index: 2;
`;

export const NavList = styled(List)`
  display: flex;
  justify-content: flex-end;
`;

export const NavLink = styled(Link)`
  color: inherit;
  &:hover { color: inherit }
`;

export const Item = styled.li`
  text-transform: uppercase;
  list-style: none;
  margin: 0 2rem;

  &:hover {
    ${pulse}
  }
`;
