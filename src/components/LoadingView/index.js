import React from 'react';
import GridAnimation from '../GridAnimation';

const LoadingView = () => <GridAnimation />;

export default LoadingView;
