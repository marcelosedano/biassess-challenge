import styled, { keyframes } from 'styled-components';
import media from 'styles/constants/media';

const grid = keyframes`
  0%, 100% {
    opacity: 1;
  }
  50% {
    opacity: 0.5;
  }
`;

export const Circle = styled.div`
  position: absolute;
  width: 13px;
  height: 13px;
  border-radius: 50%;
  background: #fff;
  animation: ${grid} 1.2s linear infinite;

  ${media.min.tablet`
    width: 19px;
    height: 19px;
  `};

  &:nth-child(1) {
    top: 6px;
    left: 6px;
    animation-delay: 0s;

    ${media.min.tablet`
      top: 10px;
      left: 10px;
    `};
  }
  &:nth-child(2) {
    top: 6px;
    left: 26px;
    animation-delay: -0.4s;

    ${media.min.tablet`
      top: 10px;
      left: 38px;
    `};
  }
  &:nth-child(3) {
    top: 6px;
    left: 45px;
    animation-delay: -0.8s;

    ${media.min.tablet`
      top: 10px;
      left: 67px;
    `};
  }
  &:nth-child(4) {
    top: 26px;
    left: 6px;
    animation-delay: -0.4s;

    ${media.min.tablet`
      top: 38px;
      left: 10px;
    `};
  }
  &:nth-child(5) {
    top: 26px;
    left: 26px;
    animation-delay: -0.8s;

    ${media.min.tablet`
      top: 38px;
      left: 38px;
    `};
  }
  &:nth-child(6) {
    top: 26px;
    left: 45px;
    animation-delay: -1.2s;

    ${media.min.tablet`
      top: 38px;
      left: 67px;
    `};
  }
  &:nth-child(7) {
    top: 45px;
    left: 6px;
    animation-delay: -0.8s;

    ${media.min.tablet`
      top: 67px;
      left: 10px;
    `};
  }
  &:nth-child(8) {
    top: 45px;
    left: 26px;
    animation-delay: -1.2s;

    ${media.min.tablet`
      top: 67px;
      left: 38px;
    `};
  }
  &:nth-child(9) {
    top: 45px;
    left: 45px;
    animation-delay: -1.6s;

    ${media.min.tablet`
      top: 67px;
      left: 67px;
    `};
  }
`;

export const Grid = styled.div`
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;

  ${media.min.tablet`
    width: 96px;
    height: 96px;
  `};
`;
