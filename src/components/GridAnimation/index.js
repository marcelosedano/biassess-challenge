import React from 'react';
import {
  Circle,
  Grid,
} from './style';

const GridAnimation = () => (
  <Grid>
    <Circle />
    <Circle />
    <Circle />
    <Circle />
    <Circle />
    <Circle />
    <Circle />
    <Circle />
    <Circle />
  </Grid>
);

export default GridAnimation;
