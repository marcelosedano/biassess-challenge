import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { injectGlobalStyles } from 'styles/global';
import App from 'components/App';
import store from 'store';

injectGlobalStyles();

const container = document.getElementById('root');

if (container) {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    container,
  );
}
