/* eslint-env jest */

import { createAction, createAsyncAction } from './index';
import createAsyncTypes from '../actionTypes/createAsyncTypes';

describe('Action creators', () => {
  describe('createAction', () => {
    const SOME_TYPE = 'SOME_TYPE';

    it('creates the expected action', () => {
      const payload = 'Some payload';
      const action = createAction(SOME_TYPE);

      expect(action(payload)).toEqual({
        type: SOME_TYPE,
        payload,
      });
    });
  });
  describe('createAsyncAction', () => {
    const SOME_ASYNC_TYPE = createAsyncTypes('SOME_ASYNC_TYPE');

    it('creates the expected async action', () => {
      const payload = 'Some payload';
      const action = createAsyncAction(SOME_ASYNC_TYPE);

      expect(action(payload)).toEqual({
        type: SOME_ASYNC_TYPE.PENDING,
        payload,
      });

      expect(action.success(payload)).toEqual({
        type: SOME_ASYNC_TYPE.SUCCESS,
        payload,
      });

      expect(action.error(payload)).toEqual({
        type: SOME_ASYNC_TYPE.ERROR,
        payload,
        error: true,
      });
    });
  });
});
