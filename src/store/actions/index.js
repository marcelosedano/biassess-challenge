import {
  GET_USER_ASYNC,
  LOGIN_ASYNC,
  LOGOUT_ASYNC,
  RESTORE_SESSION_ASYNC,
  SET_LOADING,
  TOGGLE_VIEWING_REPORT,
} from '../actionTypes';

export const createAction = type => payload => ({ payload, type });
export const createAsyncAction = type => Object.assign(
  createAction(type.PENDING),
  {
    error: payload => ({ error: true, payload, type: type.ERROR }),
    success: payload => ({ payload, type: type.SUCCESS }),
  },
);

export const getUser = createAsyncAction(GET_USER_ASYNC);
export const login = createAsyncAction(LOGIN_ASYNC);
export const logout = createAsyncAction(LOGOUT_ASYNC);
export const restoreSession = createAsyncAction(RESTORE_SESSION_ASYNC);
export const setLoading = createAction(SET_LOADING);
export const toggleViewingReport = createAction(TOGGLE_VIEWING_REPORT);
