import {
  GET_USER_ASYNC,
  LOGIN_ASYNC,
  LOGOUT_ASYNC,
  RESTORE_SESSION_ASYNC,
  SET_LOADING,
  TOGGLE_VIEWING_REPORT,
} from '../actionTypes';

export const INITIAL_STATE = {
  isLoading: true,
  isLoggedIn: false,
  isViewingReport: false,
  user: null,
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case GET_USER_ASYNC.SUCCESS:
      return {
        ...state,
        user: payload,
      };
    case LOGIN_ASYNC.SUCCESS:
    case RESTORE_SESSION_ASYNC.SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        isViewingReport: false,
      };
    case LOGOUT_ASYNC.SUCCESS:
      return {
        ...state,
        isLoggedIn: false,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case TOGGLE_VIEWING_REPORT:
      return {
        ...state,
        isViewingReport: !state.isViewingReport,
      };
    default:
      return state;
  }
};
