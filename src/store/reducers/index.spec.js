/* eslint-env jest */

import reducer, { INITIAL_STATE } from './index';
import * as types from '../actionTypes';

describe('Reducer', () => {
  it('returns the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it('handles GET_USER_ASYNC.SUCCESS', () => {
    const user = { id: '123456789' };

    expect(
      reducer(undefined, {
        type: types.GET_USER_ASYNC.SUCCESS,
        payload: user,
      }),
    ).toEqual({
      ...INITIAL_STATE,
      user,
    });
  });

  it('handles LOGIN_ASYNC.SUCCESS', () => {
    expect(
      reducer(undefined, {
        type: types.LOGIN_ASYNC.SUCCESS,
      }),
    ).toEqual({
      ...INITIAL_STATE,
      isLoggedIn: true,
    });
  });

  it('handles LOGOUT_ASYNC.SUCCESS', () => {
    expect(
      reducer(undefined, {
        type: types.LOGOUT_ASYNC.SUCCESS,
      }),
    ).toEqual({
      ...INITIAL_STATE,
      isLoggedIn: false,
    });
  });

  it('handles RESTORE_SESSION_ASYNC.SUCCESS', () => {
    expect(
      reducer(undefined, {
        type: types.RESTORE_SESSION_ASYNC.SUCCESS,
      }),
    ).toEqual({
      ...INITIAL_STATE,
      isLoggedIn: true,
    });
  });

  it('handles SET_LOADING', () => {
    const isLoading = false;

    expect(
      reducer(undefined, {
        type: types.SET_LOADING,
        payload: isLoading,
      }),
    ).toEqual({
      ...INITIAL_STATE,
      isLoading: false,
    });
  });
});
