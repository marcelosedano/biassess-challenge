import { createSelector } from 'reselect';

export const getIsLoading = createSelector(
  state => state.main.isLoading,
  isLoading => isLoading,
);

export const getIsLoggedIn = createSelector(
  state => state.main.isLoggedIn,
  isLoggedIn => isLoggedIn,
);

export const getIsViewingReport = createSelector(
  state => state.main.isViewingReport,
  isViewingReport => isViewingReport,
);

export const getUser = createSelector(
  state => state.main.user,
  user => user,
);
