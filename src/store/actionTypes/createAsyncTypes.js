export const asyncSuffixes = {
  ERROR: 'ERROR',
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
};

export default typeString => (
  Object.keys(asyncSuffixes).reduce((acc, suffixKey) => {
    const suffix = asyncSuffixes[suffixKey];

    return {
      [suffix]: `${typeString}_${suffix}`,
      ...acc,
    };
  }, {})
);
