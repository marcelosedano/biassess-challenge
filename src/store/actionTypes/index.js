import createAsyncTypes from './createAsyncTypes';

// Async types

export const GET_USER_ASYNC = createAsyncTypes('GET_USER_ASYNC');
export const LOGIN_ASYNC = createAsyncTypes('LOGIN_ASYNC');
export const LOGOUT_ASYNC = createAsyncTypes('LOGOUT_ASYNC');
export const RESTORE_SESSION_ASYNC = createAsyncTypes('RESTORE_SESSION_ASYNC');

// Sync types

export const SET_LOADING = 'SET_LOADING';
export const TOGGLE_VIEWING_REPORT = 'TOGGLE_VIEWING_REPORT';
