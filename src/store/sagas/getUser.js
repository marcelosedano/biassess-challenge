import Debug from 'debug';
import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_USER_ASYNC } from 'store/actionTypes';
import { getUser } from 'store/actions';
import FB from 'lib/fb';

const debug = Debug('biassess:store:sagas:getUser');

function* getUserSaga() {
  debug('Saga called');
  const response = yield call(FB.getUser);

  if (!response.error) {
    yield put(getUser.success(response));
  } else {
    yield put(getUser.error());
  }
}

export default function* () {
  yield takeLatest(GET_USER_ASYNC.PENDING, getUserSaga);
}
