import { fork, all } from 'redux-saga/effects';
import getUser from './getUser';
import handleLoading from './handleLoading';
import login from './login';
import logout from './logout';
import restoreSession from './restoreSession';

const sagas = [
  getUser,
  handleLoading,
  login,
  logout,
  restoreSession,
];

export default function* root() {
  yield all(sagas.map(saga => fork(saga)));
}
