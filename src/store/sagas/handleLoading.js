import {
  put,
  race,
  select,
  take,
} from 'redux-saga/effects';
import { asyncSuffixes } from 'store/actionTypes/createAsyncTypes';
import { getIsLoading } from 'store/selectors';
import { setLoading } from 'store/actions';

const PENDING_REGEX = new RegExp(`_${asyncSuffixes.PENDING}$`);
const ERROR_REGEX = new RegExp(`_${asyncSuffixes.ERROR}$`);
const SUCCESS_REGEX = new RegExp(`_${asyncSuffixes.SUCCESS}$`);

export default function* () {
  const listener = {};

  while (true) {
    const [pendingAction, errorAction, successAction] = yield race([
      take(({ type }) => PENDING_REGEX.test(type)),
      take(({ type }) => ERROR_REGEX.test(type)),
      take(({ type }) => SUCCESS_REGEX.test(type)),
    ]);

    if (pendingAction) {
      const { type } = pendingAction;
      const actionType = type.replace(PENDING_REGEX, '');

      if (!listener[actionType]) listener[actionType] = 0;
      listener[actionType] += 1;
    }

    if (successAction || errorAction) {
      const { type } = successAction || errorAction;
      const actionType = type.replace(ERROR_REGEX, '').replace(SUCCESS_REGEX, '');

      if (listener[actionType]) listener[actionType] -= 1;
    }

    const isLoading = yield select(getIsLoading);
    const shouldBeLoading = Object.keys(listener).some(key => listener[key]);

    if (isLoading !== shouldBeLoading) yield put(setLoading(shouldBeLoading));
  }
}
