import Debug from 'debug';
import { call, put, takeLatest } from 'redux-saga/effects';
import { LOGIN_ASYNC } from 'store/actionTypes';
import { getUser, login } from 'store/actions';
import FB from 'lib/fb';

const debug = Debug('biassess:store:sagas:login');

function* loginSaga() {
  debug('Saga called');
  const scopes = ['email'];
  const response = yield call(FB.login, { scope: scopes.join(',') });

  if (response.status === 'connected') {
    yield put(getUser());
    yield put(login.success());
  } else {
    yield put(login.error());
  }
}

export default function* () {
  yield takeLatest(LOGIN_ASYNC.PENDING, loginSaga);
}
