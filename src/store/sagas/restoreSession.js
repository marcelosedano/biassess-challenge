import Debug from 'debug';
import { call, put, takeLatest } from 'redux-saga/effects';
import { RESTORE_SESSION_ASYNC } from 'store/actionTypes';
import { getUser, restoreSession } from 'store/actions';
import FB from 'lib/fb';

const debug = Debug('biassess:store:sagas:restoreSession');

function* restoreSessionSaga() {
  debug('Saga called');
  const response = yield call(FB.getLoginStatus);

  if (response.status === 'connected') {
    yield put(getUser());
    yield put(restoreSession.success());
  } else {
    yield put(restoreSession.error());
  }
}

export default function* () {
  yield takeLatest(RESTORE_SESSION_ASYNC.PENDING, restoreSessionSaga);
}
