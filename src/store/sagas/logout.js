import Debug from 'debug';
import { call, put, takeLatest } from 'redux-saga/effects';
import { LOGOUT_ASYNC } from 'store/actionTypes';
import { logout } from 'store/actions';
import FB from 'lib/fb';

const debug = Debug('biassess:store:sagas:logout');

function* logoutSaga() {
  debug('Saga called');
  const response = yield call(FB.logout);

  if (response.status === 'unknown') {
    yield put(logout.success());
  }
}

export default function* () {
  yield takeLatest(LOGOUT_ASYNC.PENDING, logoutSaga);
}
