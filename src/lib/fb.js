import Debug from 'debug';

const debug = Debug('biassess:lib:fb');

const getLoginStatus = () => new Promise(resolve => {
  debug('getLoginStatus called');
  window.FB.getLoginStatus(response => {
    debug('getLoginStatus:response', response);
    resolve(response);
  });
});

const getUser = () => new Promise(resolve => {
  debug('getUser called');
  window.FB.api('/me?fields=email,name,picture.type(large)', response => {
    debug('getUser:response', response);
    resolve(response);
  });
});

const login = options => new Promise(resolve => {
  debug('Login called with options:', options);
  window.FB.login(response => {
    debug('login:response', response);
    resolve(response);
  }, options);
});

const logout = () => new Promise(resolve => {
  debug('Logout called');
  window.FB.logout(response => {
    debug('logout:response', response);
    resolve(response);
  });
});

export default {
  getLoginStatus,
  getUser,
  login,
  logout,
};
